# Cancun api

![CI Status](https://github.com/lomasz/spring-boot-template/workflows/CI/badge.svg)
![CodeQL Status](https://github.com/lomasz/spring-boot-template/workflows/CodeQL/badge.svg)

* Spring Boot + Web + JPA + REST
* Marven
* Lombok
* Flyway Migration
* Mapstruct
* Swagger
* Java 8
* PostgreSQL 14
* JUnit 5
* AssertJ

## :gear: Configurations
- [ ]Le profil pour tester l'api est [`dev`](src/main/resources/application-dev.yml.template):
    * Télécharger le fichier de connexion a la bd est **`application-dev.yml.template`**


- [ ] Télécharger du dépôt principal[`application-dev.yml.template`](src/main/resources/application-dev.yml.template):
    * Renommer le fichier**`application-dev.yml.template`** en **`application-secret.yml`** 
    * changer si nécessaire la valeur de  **`Cancun api port`** 
      ```javascript
      server:
        port: XXXX
      ```
    * changer si nécessaire la valeur de **`PostgreSQL: Port, Username, Password`** 
      ```javascript
        datasource:
          url: jdbc:postgresql://localhost:XXXX/cmrdb?currentSchema=cancun,public
          driver-class-name: org.postgresql.Driver
          username: XXXX
          password: XXXX
      ```

- [x] Télécharger les dépendances
    * Lancer `mvn clean install` dans l'invite de commande si Marven est installé localement
    * Vous pouvez aussi gérer les dépendances en utilisant  [`IntelliJ`](https://www.jetbrains.com/idea/guide/tutorials/migrating-javax-jakarta/update-dependencies/)


## API Documentation
**`Cancun api default port: 9000`**
* [`http://localhost:9000/api-docs`](http://localhost:9000/api-docs) - API Docs [JSON]
* [`http://localhost:9000/swagger-ui.html`](http://localhost:9000/swagger-ui.html) - Swagger UI

**`Docs capture:`**
![picture alt](src/main/resources/images/swagger.png "Title is optional")
