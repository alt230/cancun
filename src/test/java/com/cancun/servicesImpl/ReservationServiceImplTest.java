package com.cancun.servicesImpl;


import com.cancun.dto.ReservationDto;
import com.cancun.entities.Chambre;
import com.cancun.entities.Reservation;
import com.cancun.entities.model.Common;
import com.cancun.entities.model.CustumMockBean;
import com.cancun.exceptions.ResourceAlertException;
import com.cancun.exceptions.ResourceNotFoundException;
import com.cancun.exceptions.ResourceUnAvailableException;
import com.cancun.repositories.ChambreRepository;
import com.cancun.repositories.ReservationRepository;
import com.cancun.services.ChambreService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReservationServiceImplTest {

    @Mock
    ReservationRepository reservationRepository;

    @Mock
    ChambreRepository chambreRepository;
    private ReservationServiceImpl reservationService;
    private ChambreService chambreService;


    @BeforeEach
    public void setup() {
        chambreService = new ChambreServiceImpl(chambreRepository);
        reservationService = new ReservationServiceImpl(chambreService,reservationRepository);
    }

    @Test
    @DisplayName("Ajouter une reservation dans la bd")
    void addReservation() throws ParseException {
        Chambre chambre = Chambre.builder()
                .id(1L)
                .typeLogement("Chambre Double")
                .description("description type logement Chambre Double")
                .superficie(15)
                .nonFumeur(true)
                .disponible(true)
                .montant(2500F)
                .build();
        when(chambreRepository.save(chambre)).thenReturn(chambre);
        Chambre chambreEnregistrer = chambreService.addOrUpdateRoom(chambre);
        Reservation reservation = Reservation.builder()
                .id(1L)
                .dateReservation(Common.dateToLocalDateTime(Common.formatStringToDate("2023-01-16")))
                .dateFin(Common.dateToLocalDateTime(Common.formatStringToDate("2023-01-19")))
                .chambre(chambreEnregistrer)
                .build();
        when(reservationRepository.save(reservation)).thenReturn(reservation);

        Reservation reservationAEnregistrer = reservationService.addOrUpdateReservation(reservation);
        assertThat(reservationAEnregistrer).isNotNull();
    }


    @Test
    @DisplayName("recuperer une reservation en fonction de son id")
    void findReservationById() throws ParseException {
        Chambre chambre = Chambre.builder()
                .id(1L)
                .typeLogement("Chambre Double")
                .description("description type logement Chambre Double")
                .superficie(15)
                .nonFumeur(true)
                .disponible(true)
                .montant(2500F)
                .build();
        when(chambreRepository.save(chambre)).thenReturn(chambre);
        Chambre chambreEnregistrer = chambreService.addOrUpdateRoom(chambre);
        Reservation reservation = Reservation.builder()
                .id(1L)
                .dateReservation(Common.dateToLocalDateTime(Common.formatStringToDate("2023-01-16")))
                .dateFin(Common.dateToLocalDateTime(Common.formatStringToDate("2023-01-19")))
                .chambre(chambreEnregistrer)
                .build();
        when(reservationRepository.findById(1L)).thenReturn(Optional.of(reservation));
        //WHEN
        Optional<Reservation> reservation1 = reservationService.findReservationById(reservation.getId());
        //THEN
        assertThat(reservation1.get()).isEqualTo(reservation);
    }

    @Test
    void findAllReservations() throws ParseException {
        //GIVEN
        Chambre chambre = Chambre.builder()
                .id(1L)
                .typeLogement("Chambre Double")
                .description("description type logement Chambre Double")
                .superficie(15)
                .nonFumeur(true)
                .disponible(true)
                .montant(2500F)
                .build();

        when(chambreRepository.findById(1L)).thenReturn(Optional.of(chambre));
        Optional<Chambre> chambreEnregistrer = chambreService.findRoomById(chambre.getId());
        Reservation reservation = Reservation.builder()
                .id(1L)
                .dateReservation(Common.dateToLocalDateTime(Common.formatStringToDate("2023-01-16")))
                .dateFin(Common.dateToLocalDateTime(Common.formatStringToDate("2023-01-19")))
                .chambre(chambreEnregistrer.get())
                .build();
        List<Reservation> reservations = Arrays.asList(reservation);
        when(reservationRepository.findAll()).thenReturn(reservations);


        //WHEN
        List<Reservation> ReservationEnregistrer = reservationService.findAllReservations();

        //THEN
        assertThat(ReservationEnregistrer.size()).isEqualTo(reservations.size());
    }


    @Test
    @DisplayName("recuperer reservations disponible liste vide")
    void recupererReservationsDisponibleEmptyList() throws ParseException {
        //GIVEN
        Chambre chambre = Chambre.builder()
                .id(1L)
                .typeLogement("Chambre Double")
                .description("description type logement Chambre Double")
                .superficie(15)
                .nonFumeur(true)
                .disponible(false)
                .montant(2500F)
                .build();

        when(chambreRepository.findById(1L)).thenReturn(Optional.of(chambre));
        Optional<Chambre> chambreEnregistrer = chambreService.findRoomById(chambre.getId());
        Reservation reservation = Reservation.builder()
                .id(1L)
                .dateReservation(Common.dateToLocalDateTime(Common.formatStringToDate("2023-01-16")))
                .dateFin(Common.dateToLocalDateTime(Common.formatStringToDate("2023-01-19")))
                .chambre(chambreEnregistrer.get())
                .annule(true)
                .build();
        List<Reservation> reservations = Arrays.asList(reservation);
        when(reservationRepository.findAll()).thenReturn(reservations);


        //WHEN
        ResponseEntity<?>  ReservationEnregistrer = reservationService.recupererReservations();

        //THEN
        assertThat(ReservationEnregistrer).isNotNull();
        assertThat(ReservationEnregistrer.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @DisplayName("recuperer reservation disponible")
    void recupererReservationsDisponibleNotEmptyList() throws ParseException {
        //GIVEN
        Chambre chambre = Chambre.builder()
                .id(1L)
                .typeLogement("Chambre Double")
                .description("description type logement Chambre Double")
                .superficie(15)
                .nonFumeur(true)
                .disponible(true)
                .montant(2500F)
                .build();

        when(chambreRepository.findById(1L)).thenReturn(Optional.of(chambre));
        Optional<Chambre> chambreEnregistrer = chambreService.findRoomById(chambre.getId());
        Reservation reservation = Reservation.builder()
                .id(1L)
                .dateReservation(Common.dateToLocalDateTime(Common.formatStringToDate("2023-01-16")))
                .dateFin(Common.dateToLocalDateTime(Common.formatStringToDate("2023-01-19")))
                .chambre(chambreEnregistrer.get())
                .annule(false)
                .build();
        List<Reservation> reservations = Arrays.asList(reservation);
        when(reservationRepository.findAll()).thenReturn(reservations);
        //WHEN
        ResponseEntity<?>  ReservationEnregistrer = reservationService.recupererReservations();
        //THEN
        assertThat(ReservationEnregistrer).isNotNull();
        assertThat(ReservationEnregistrer.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(ReservationEnregistrer.getBody()).isNotNull();
    }

    @Test
    void recupererReservation() {
    }

    @Test
    void annulerReservation() {
    }

    @Test
    void modifierReservation() {
    }

    @Test
    void effectuerUneReservation() {
    }


    @Test
    @DisplayName("recuperer reservation avec id negatif")
    void recupererReservationIdNegative() {
        //GIVEN
        Long reservationId = -2L;
        //THEN
        assertThrows(ResourceNotFoundException.class, () -> reservationService.recupererReservation(reservationId));
    }

    @Test
    @DisplayName("recuperer reservation avec id 404")
    void recupererReservationIdNotFoundTest() {
        //GIVEN
        Long roomId = 12L;
        //THEN
        assertThrows(ResourceNotFoundException.class, () -> reservationService.recupererReservation(roomId));
    }

    @Test
    @DisplayName("recuperer reservation indisponible")
    void recupererReservationIndisponibleTest() throws ParseException {
        //GIVEN
        Chambre chambre = Chambre.builder()
                .id(1L)
                .typeLogement("Chambre Double")
                .description("description type logement Chambre Double")
                .superficie(15)
                .nonFumeur(true)
                .disponible(false)
                .montant(2500F)
                .build();

        when(chambreRepository.findById(1L)).thenReturn(Optional.of(chambre));
        Optional<Chambre> chambreEnregistrer = chambreService.findRoomById(chambre.getId());
        Reservation reservation = Reservation.builder()
                .id(1L)
                .dateReservation(Common.dateToLocalDateTime(Common.formatStringToDate("2023-01-16")))
                .dateFin(Common.dateToLocalDateTime(Common.formatStringToDate("2023-01-19")))
                .chambre(chambreEnregistrer.get())
                .annule(true)
                .build();
        when(reservationRepository.findById(1L)).thenReturn(Optional.of(reservation));
        //WHEN
        ResponseEntity<?> reservationEnregistre = reservationService.recupererReservation(reservation.getId());
        //THEN
        assertThat(reservationEnregistre).isNotNull();
        assertThat(reservationEnregistre.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(reservationEnregistre.getBody()).isNotNull();
    }

    @Test
    @DisplayName("recuperer reservation avec id null")
    void recupererReservationIdNULLTest() {
        Long id = null;
        //THEN
        assertThrows(NullPointerException.class, () -> reservationService.recupererReservation(id));
    }

    @Test
    @DisplayName("recuperer reservation Ok")
    void recupererReservationOKTest() throws ParseException {
        Chambre chambre = Chambre.builder()
                .id(1L)
                .typeLogement("Chambre Double")
                .description("description type logement Chambre Double")
                .superficie(15)
                .nonFumeur(true)
                .disponible(true)
                .montant(2500F)
                .build();

        when(chambreRepository.findById(1L)).thenReturn(Optional.of(chambre));
        Optional<Chambre> chambreEnregistrer = chambreService.findRoomById(chambre.getId());
        Reservation reservation = Reservation.builder()
                .id(1L)
                .dateReservation(Common.dateToLocalDateTime(Common.formatStringToDate("2023-01-16")))
                .dateFin(Common.dateToLocalDateTime(Common.formatStringToDate("2023-01-19")))
                .chambre(chambreEnregistrer.get())
                .annule(false)
                .build();
        when(reservationRepository.findById(1L)).thenReturn(Optional.of(reservation));
        //WHEN
        ResponseEntity<?> reservationEnregistre = reservationService.recupererReservation(reservation.getId());
        //THEN
        assertThat(reservationEnregistre).isNotNull();
        assertThat(reservationEnregistre.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(reservationEnregistre.getBody()).isNotNull();
    }

    @Test
    @DisplayName("annuler reservation avec id negatif ou egale a 0")
    void annulerReservationIdNegativeOuZEROTest() {
        //GIVEN
        Long reservationId = -2L;
        //THEN
        assertThrows(ResourceAlertException.class, () -> reservationService.annulerReservation(reservationId));
    }

    @Test
    @DisplayName("annuler reservation avec id 404")
    void annulerReservationIdNotFoundTest() {
        //GIVEN
        Long reservationId = 12L;
        //THEN
        assertThrows(ResourceNotFoundException.class, () -> reservationService.annulerReservation(reservationId));
    }


    @Test
    @DisplayName("annuler reservation indisponible")
    void annulerReservationIndisponibleTest() throws ParseException {
        //GIVEN
        Chambre chambre = Chambre.builder()
                .id(1L)
                .typeLogement("Chambre Double")
                .description("description type logement Chambre Double")
                .superficie(15)
                .nonFumeur(true)
                .disponible(false)
                .montant(2500F)
                .build();

        when(chambreRepository.findById(1L)).thenReturn(Optional.of(chambre));
        Optional<Chambre> chambreEnregistrer = chambreService.findRoomById(chambre.getId());
        Reservation reservation = Reservation.builder()
                .id(1L)
                .dateReservation(Common.dateToLocalDateTime(Common.formatStringToDate("2023-01-16")))
                .dateFin(Common.dateToLocalDateTime(Common.formatStringToDate("2023-01-19")))
                .chambre(chambreEnregistrer.get())
                .annule(true)
                .build();
        when(reservationRepository.findById(1L)).thenReturn(Optional.of(reservation));

        //THEN
        assertThrows(ResourceUnAvailableException.class, () -> reservationService.annulerReservation(reservation.getId()));
    }

    @Test
    @DisplayName("annuler reservation Ok")
    void annulerReservationOKTest() throws ParseException {
        //GIVEN
        Chambre chambre = Chambre.builder()
                .id(1L)
                .typeLogement("Chambre Double")
                .description("description type logement Chambre Double")
                .superficie(15)
                .nonFumeur(true)
                .disponible(false)
                .montant(2500F)
                .build();

        when(chambreRepository.findById(1L)).thenReturn(Optional.of(chambre));
        Optional<Chambre> chambreEnregistrer = chambreService.findRoomById(chambre.getId());
        Reservation reservation = Reservation.builder()
                .id(1L)
                .dateReservation(Common.dateToLocalDateTime(Common.formatStringToDate("2023-01-16")))
                .dateFin(Common.dateToLocalDateTime(Common.formatStringToDate("2023-01-19")))
                .chambre(chambreEnregistrer.get())
                .annule(false)
                .build();
        when(reservationRepository.findById(1L)).thenReturn(Optional.of(reservation));
        //WHEN
        ResponseEntity<?> chambreEnregistre = reservationService.annulerReservation(reservation.getId());
        //THEN
        assertThat(chambreEnregistre).isNotNull();
        assertThat(chambreEnregistre.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(chambreEnregistre.getBody()).isNotNull();
    }


    @Test
    @DisplayName("modifier reservation avec id negatif ou egale a 0")
    void modifierReservationIdNegativeOuNullTest() throws ParseException {
        //GIVEN
        Chambre chambre = Chambre.builder()
                .id(1L)
                .typeLogement("Chambre Double")
                .description("description type logement Chambre Double")
                .superficie(15)
                .nonFumeur(true)
                .disponible(true)
                .montant(2500F)
                .build();

        when(chambreRepository.findById(1L)).thenReturn(Optional.of(chambre));
        Optional<Chambre> chambreEnregistrer = chambreService.findRoomById(chambre.getId());
       ReservationDto reservationDto = ReservationDto.builder()
               .dateReservation("2023-01-16")
               .dateDepart("2023-01-19")
               .chambreId(chambreEnregistrer.get().getId())
               .id(-2L)
               .build();
        //THEN
        assertThrows(ResourceAlertException.class, () -> reservationService.modifierReservation(reservationDto));
    }

    @Test
    @DisplayName("modifier reservation avec id negatif ou egale a 0")
    void modifierReservationIdNotFoundTest() throws ParseException {
        //GIVEN
        Chambre chambre = Chambre.builder()
                .id(1L)
                .typeLogement("Chambre Double")
                .description("description type logement Chambre Double")
                .superficie(15)
                .nonFumeur(true)
                .disponible(true)
                .montant(2500F)
                .build();

        when(chambreRepository.findById(1L)).thenReturn(Optional.of(chambre));
        Optional<Chambre> chambreEnregistrer = chambreService.findRoomById(chambre.getId());
        ReservationDto reservationDto = ReservationDto.builder()
                .dateReservation("2023-01-16")
                .dateDepart("2023-01-19")
                .chambreId(chambreEnregistrer.get().getId())
                .id(12L)
                .build();
        //THEN
        assertThrows(ResourceNotFoundException.class, () -> reservationService.modifierReservation(reservationDto));
    }
}