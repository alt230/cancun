package com.cancun.servicesImpl;

import com.cancun.dto.ChambreDto;
import com.cancun.entities.Chambre;
import com.cancun.entities.model.CustumMockBean;
import com.cancun.exceptions.ResourceNotFoundException;
import com.cancun.exceptions.ResourceUnAvailableException;
import com.cancun.repositories.ChambreRepository;
import com.cancun.utils.CustumMessage;
import org.assertj.core.api.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ChambreServiceImplTest {
    @Mock
    ChambreRepository chambreRepository;
    private ChambreServiceImpl chambreService;

    @BeforeEach
    public void setup() {
        chambreService = new ChambreServiceImpl(chambreRepository);
    }

    @Test
    @DisplayName("recuperer une chambre en fonction de son id")
    void findRoomById() {
        Chambre chambre = Chambre.builder()
                .id(1L)
                .typeLogement("Chambre Double")
                .description("description type logement Chambre Double")
                .superficie(15)
                .nonFumeur(true)
                .disponible(true)
                .montant(2500F)
                .build();
        when(chambreRepository.findById(1L)).thenReturn(Optional.of(chambre));
        //WHEN
        Optional<Chambre> room = chambreService.findRoomById(chambre.getId());
        //THEN
        assertThat(room.get()).isEqualTo(chambre);
    }

    @Test
    @DisplayName("Ajouter ou mettre a jour une chambre dans la bd")
    void addOrUpdateRoom() {
        Chambre chambre = Chambre.builder()
                .id(1L)
                .typeLogement("Chambre Double")
                .description("description type logement Chambre Double")
                .superficie(15)
                .nonFumeur(true)
                .disponible(true)
                .montant(2500F)
                .build();
        when(chambreRepository.save(chambre)).thenReturn(chambre);
        //WHEN
        Chambre chambreAEnregistrer = chambreService.addOrUpdateRoom(chambre);
        //THEN
        assertThat(chambreAEnregistrer).isNotNull();
    }

    @Test
    @DisplayName("Ajouter une liste de chambre dans la bd")
    void saveAll() {
        //GIVEN
        Chambre chambre = CustumMockBean.getInstance().chambreBean();
        Chambre chambre2 = chambre;
        List<Chambre> chambres = Arrays.asList(chambre, chambre2);
        when(chambreRepository.saveAll(chambres)).thenReturn(chambres);
        //WHEN
        List<Chambre> rooms = chambreService.saveAll(chambres);
        //THEN
        assertThat(rooms.size()).isEqualTo(2);
    }

    @Test
    @DisplayName("recuperer  toutes les chambres")
    void recupererChambres() {
        //GIVEN
        Chambre chambre = CustumMockBean.getInstance().chambreBean();
        Chambre chambre2 = chambre;
        List<Chambre> chambres = Arrays.asList(chambre, chambre2);
        when(chambreRepository.findAll()).thenReturn(chambres);
        //WHEN
        List<Chambre> rooms = chambreService.recupererChambres();

        //THEN
        assertThat(rooms.size()).isEqualTo(2);
    }

    @Test
    @DisplayName("recuperer chambres disponible liste vide")
    void recupererChambresDisponibleEmptyList() {
        //GIVEN
        Chambre chambre = CustumMockBean.getInstance().chambreUnAvailableBean();
        Chambre chambre2 = chambre;
        List<Chambre> chambres = Arrays.asList(chambre, chambre2);
        when(chambreRepository.findAll()).thenReturn(chambres);
        //WHEN
        ResponseEntity<?> rooms = chambreService.recupererChambresDisponible();

        //THEN
        assertThat(rooms).isNotNull();
        assertThat(rooms.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    @DisplayName("recuperer chambres disponible")
    void recupererChambresDisponibleNotEmptyList() {
        //GIVEN
        Chambre chambre = CustumMockBean.getInstance().chambreBean();
        Chambre chambre2 = chambre;
        List<Chambre> chambres = Arrays.asList(chambre, chambre2);
        when(chambreRepository.findAll()).thenReturn(chambres);
        //WHEN
        ResponseEntity<?> rooms = chambreService.recupererChambresDisponible();

        //THEN
        assertThat(rooms).isNotNull();
        assertThat(rooms.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(rooms.getBody()).isNotNull();
    }

    @Test
    @DisplayName("recuperer chambre avec id negatif")
    void recupererChambreIdNegative() {
        //GIVEN
        Long roomId = -2L;
        //THEN
        assertThrows(ResourceNotFoundException.class, () -> chambreService.recupererChambre(roomId));
    }


    @Test
    @DisplayName("recuperer chambre avec id 404")
    void recupererChambreIdNotFoundTest() {
        //GIVEN
        Long roomId = 12L;
        //THEN
        assertThrows(ResourceNotFoundException.class, () -> chambreService.recupererChambre(roomId));
    }

    @Test
    @DisplayName("recuperer chambre indisponible")
    void recupererChambreIndisponibleTest() {
        //GIVEN
        Chambre chambre = Chambre.builder()
                .id(1L)
                .typeLogement("Chambre Double")
                .description("description type logement Chambre Double")
                .superficie(15)
                .nonFumeur(true)
                .disponible(false)
                .montant(2500F)
                .build();
        when(chambreRepository.findById(1L)).thenReturn(Optional.of(chambre));
        //THEN
        assertThrows(ResourceUnAvailableException.class, () -> chambreService.recupererChambre(chambre.getId()));
    }

    @Test
    @DisplayName("recuperer chambre avec id null")
    void recupererChambreIdNULLTest() {
        Chambre chambre = CustumMockBean.getInstance().chambreBean();
        //THEN
        assertThrows(NullPointerException.class, () -> chambreService.recupererChambre(chambre.getId()));
    }

    @Test
    @DisplayName("recuperer chambre Ok")
    void recupererChambreOKTest() {
        Chambre chambre = Chambre.builder()
                .id(2L)
                .typeLogement("Chambre Double")
                .description("description type logement Chambre Double")
                .superficie(15)
                .nonFumeur(true)
                .disponible(true)
                .montant(2500F)
                .build();
        when(chambreRepository.findById(2L)).thenReturn(Optional.of(chambre));
        //WHEN
        ResponseEntity<?> chambreEnregistre = chambreService.recupererChambre(chambre.getId());
        //THEN
        assertThat(chambreEnregistre).isNotNull();
        assertThat(chambreEnregistre.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(chambreEnregistre.getBody()).isNotNull();
    }
}