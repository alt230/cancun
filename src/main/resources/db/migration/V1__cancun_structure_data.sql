-- Table: chambre

-- DROP TABLE chambre;

CREATE TABLE chambre
(
    id  SERIAL NOT NULL,
    date_creation timestamp without time zone,
    date_mis_ajour timestamp without time zone,
    description character varying(255),
    disponible boolean NOT NULL,
    montant real NOT NULL,
    nombre_de_jour_reservation integer NOT NULL,
    non_fumeur boolean NOT NULL,
    superficie double precision NOT NULL,
    type_logement character varying(255),
    CONSTRAINT chambre_pkey PRIMARY KEY (id)
);


-- Table: chambre_equipements

-- DROP TABLE chambre_equipements;

CREATE TABLE chambre_equipements
(
    chambre_id bigint NOT NULL,
    equipement character varying(255),
    CONSTRAINT fk7demkextamvw1ggl27ncc5a9t FOREIGN KEY (chambre_id)
        REFERENCES chambre (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);


-- Table: chambre_options

-- DROP TABLE chambre_options;

CREATE TABLE chambre_options
(
    chambre_id bigint NOT NULL,
    option_salle_de_bain character varying(255),
    CONSTRAINT fk43cxkvffc2tvm2ndli931i0io FOREIGN KEY (chambre_id)
        REFERENCES chambre (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);


-- Table: chambre_vues

-- DROP TABLE chambre_vues;

CREATE TABLE chambre_vues
(
    chambre_id bigint NOT NULL,
    vue character varying(255),
    CONSTRAINT fk7onch1ha6cvugalpvl6ii2kan FOREIGN KEY (chambre_id)
        REFERENCES chambre (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);


-- Table: client

-- DROP TABLE client;

CREATE TABLE client
(
    id  SERIAL NOT NULL,
    date_creation timestamp without time zone,
    date_mis_ajour timestamp without time zone,
    civilite character varying(255),
    date_arrivee timestamp without time zone,
    email character varying(255),
    nom character varying(255) NOT NULL,
    prenom character varying(255),
    telephone character varying(255),
    CONSTRAINT client_pkey PRIMARY KEY (id)
);


-- Table: reservation

-- DROP TABLE reservation;

CREATE TABLE reservation
(
    id  SERIAL NOT NULL,
    date_creation timestamp without time zone,
    date_mis_ajour timestamp without time zone,
    annule boolean,
    date_annulation timestamp without time zone,
    date_debut timestamp without time zone NOT NULL,
    date_fin timestamp without time zone,
    date_reservation timestamp without time zone NOT NULL,
    duree_sejour integer NOT NULL,
    chambre_id bigint NOT NULL,
    CONSTRAINT reservation_pkey PRIMARY KEY (id),
    CONSTRAINT fkdqj45imhck9x1xd2b8l3oi05t FOREIGN KEY (chambre_id)
        REFERENCES chambre (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

-- Table: client_reservation

-- DROP TABLE client_reservation;

CREATE TABLE client_reservation
(

    id SERIAL NOT NULL,
    date_creation timestamp without time zone,
    date_mis_ajour timestamp without time zone,
    client_id bigint NOT NULL,
    reservation_id bigint NOT NULL,
    CONSTRAINT client_reservation_pkey PRIMARY KEY (id),
    CONSTRAINT fkir78635a9kd9d1otmijsn1tb3 FOREIGN KEY (reservation_id)
        REFERENCES reservation (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fkkrw4foxwkn4qblkyw5lagq6o FOREIGN KEY (client_id)
        REFERENCES client (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);