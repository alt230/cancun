INSERT INTO chambre (date_creation,date_mis_ajour,description,disponible,montant,nombre_de_jour_reservation,non_fumeur,superficie,type_logement) VALUES
	 ('2023-01-08 18:03:07.824061','2023-01-08 18:03:07.885023','1 lit simple Lits confortables, notés 9 (d''après 104 commentaires) Air-conditioned room featuring an LCD TV, a phone and a private bathroom with shower and hairdryer.',true,1850.0,0,true,15.0,'Chambre Simple'),
	 ('2023-01-08 18:03:07.856041','2023-01-08 18:03:07.937991','1 grand lit double Lits confortables, notés 9 (d''après 104 commentaires) Air-conditioned room featuring an LCD TV, a phone and a private bathroom with shower and hairdryer.',true,2500.0,0,true,15.0,'Chambre Double'),
	 ('2023-01-08 18:03:07.861039','2023-01-08 18:03:07.941989','1 grand lit double Lits confortables, notés 9 (d''après 104 commentaires) This suite has air conditioning.',true,2000.0,0,true,20.0,'Suite Junior'),
	 ('2023-01-08 18:03:07.867036','2023-01-08 18:03:07.952982','3 lits simples Lits confortables, notés 9 (d''après 104 commentaires) Air-conditioned room featuring an LCD TV, a phone and a private bathroom with shower and hairdryer.',true,3500.0,0,true,15.0,'Chambre Triple'),
	 ('2023-01-08 18:03:07.873032','2023-01-08 18:03:07.95798','4 lits simples Lits confortables, notés 9 (d''après 104 commentaires) This family room features air conditioning.',false,1500.0,0,true,35.0,'Chambre Familiale');



INSERT INTO chambre_equipements (chambre_id,equipement) VALUES
	 (1,'CLIMATISATION'),
	 (1,'CHAUFFAGE'),
	 (1,'BUREAU'),
	 (1,'TELEVISION'),
	 (1,'TELEPHONE'),
	 (1,'MINIBAR'),
	 (1,'SERVICE_REVEIL'),
	 (1,'CHAINE_CABLE'),
	 (2,'CLIMATISATION'),
	 (2,'CHAUFFAGE'),
	 (2,'BUREAU'),
	 (2,'TELEVISION'),
	 (2,'TELEPHONE'),
	 (2,'MINIBAR'),
	 (2,'SERVICE_REVEIL'),
	 (2,'TRES_GRAND_LIT'),
	 (2,'CHAINE_CABLE'),
	 (2,'COFFRE_FORT'),
	 (3,'CLIMATISATION'),
	 (3,'CHAUFFAGE'),
	 (3,'BUREAU'),
	 (3,'TELEVISION'),
	 (3,'TELEPHONE'),
	 (4,'CLIMATISATION'),
	 (4,'CHAUFFAGE'),
	 (4,'BUREAU'),
	 (4,'TELEVISION'),
	 (4,'TELEPHONE'),
	 (4,'SERVICE_REVEIL'),
	 (4,'TRES_GRAND_LIT'),
	 (4,'CHAINE_CABLE'),
	 (5,'CLIMATISATION'),
	 (5,'CHAUFFAGE'),
	 (5,'BUREAU'),
	 (5,'TELEVISION'),
	 (5,'TELEPHONE');


INSERT INTO chambre_options (chambre_id,option_salle_de_bain) VALUES
	 (1,'DOUCHE'),
	 (1,'TOILETTES'),
	 (1,'SECHE_CHEVEUX'),
	 (2,'DOUCHE'),
	 (2,'TOILETTES'),
	 (2,'SECHE_CHEVEUX'),
	 (3,'DOUCHE'),
	 (3,'TOILETTES'),
	 (3,'SECHE_CHEVEUX'),
	 (4,'DOUCHE'),
	 (4,'PEIGNOIR'),
	 (4,'TOILETTES'),
	 (4,'CHAUSSONS'),
	 (4,'SECHE_CHEVEUX'),
	 (5,'DOUCHE'),
	 (5,'TOILETTES'),
	 (5,'SECHE_CHEVEUX');

INSERT INTO client (date_creation,date_mis_ajour,civilite,date_arrivee,email,nom,prenom,telephone) VALUES
	 ('2023-01-08 18:03:08.102891','2023-01-08 18:03:08.102891','Mr','2023-01-08 18:03:08.09','patricknoah2010@gmail.com','Noah','Patrick','58311155'),
	 ('2023-01-08 18:03:08.112884','2023-01-08 18:03:08.112884','Mr','2023-01-08 18:03:08.09','isaacnewton@gmail.com','Newton','Isaac','58311156');



