package com.cancun.servicesImpl;

import com.cancun.dto.ChambreResponseDto;
import com.cancun.entities.Chambre;
import com.cancun.entities.model.Common;
import com.cancun.exceptions.ResourceNotFoundException;
import com.cancun.exceptions.ResourceUnAvailableException;
import com.cancun.repositories.ChambreRepository;
import com.cancun.services.ChambreService;
import com.cancun.utils.CustumApiResponse;
import com.cancun.utils.CustumMessage;
import com.cancun.utils.CustumStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Transactional
@Service
@RequiredArgsConstructor
public class ChambreServiceImpl implements ChambreService {

    @Autowired
    private final ChambreRepository chambreRepository;


    @Override
    public Optional<Chambre> findRoomById(Long id) {
        return chambreRepository.findById(id);
    }

    @Override
    public Chambre addOrUpdateRoom(Chambre chambre) {
        return chambreRepository.save(chambre);
    }

    @Override
    public List<Chambre> saveAll(List<Chambre> chambres) {
            return chambreRepository.saveAll(chambres);
    }

    @Override
    public List<Chambre> recupererChambres() {
        return chambreRepository.findAll();
    }

    @Override
    public ResponseEntity<?> recupererChambresDisponible() {
        String message;
        List<Chambre> chambres= chambreRepository.findAll().stream()
                .filter(Chambre::isDisponible)
                .collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(chambres)) {
            message = CustumMessage.OPERATION_SUCCESS;
            return ResponseEntity.ok(new CustumApiResponse(Common.mapList(chambres, ChambreResponseDto.class), message, CustumStatus.OK));
        } else {
            message = CustumMessage.LISTE_VIDE;
            return ResponseEntity.ok(new CustumApiResponse(new ArrayList<>(), message, CustumStatus.NO_CONTENT));
        }
    }

    /**
     * Recuperation des détails de la chambre
     * @param idChambre
     * @return
     */
    @Override
    public ResponseEntity<?> recupererChambre(Long idChambre) {
        String message;
        if (idChambre <= 0L) {
            message = CustumMessage.SELECTIONNER_CHAMBRE;
            throw new ResourceNotFoundException(message);
        }
        Optional<Chambre> optionalChambre = chambreRepository.findById(idChambre);
        if (!optionalChambre.isPresent()) {
            message = CustumMessage.DONNEE_INTROUVABLE;
            throw new ResourceNotFoundException(message);
        } else {
            if (!optionalChambre.get().isDisponible()) {
                message = CustumMessage.CHAMBRE_INDISPONIBLE;
                throw new ResourceUnAvailableException(message);
            } else {
                message = CustumMessage.OPERATION_SUCCESS;
                return ResponseEntity.ok(new CustumApiResponse(optionalChambre.get(), message, CustumStatus.OK));
            }

        }
    }
}
