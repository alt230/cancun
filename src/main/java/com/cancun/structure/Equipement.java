package com.cancun.structure;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Equipement {

    CLIMATISATION("Climatisation"),
    CHAUFFAGE("Chauffage"),
    BUREAU("Bureau"),
    TELEVISION("Télévision"),
    TELEPHONE("Téléphone"),
    MINIBAR("Minibar"),
    SERVICE_REVEIL("Service de réveil / réveil"),
    TRES_GRAND_LIT("Très grands lits (> 2 mètres de long)"),
    CHAINE_CABLE("Chaînes du câble"),
    REFRIGERATEUR("Réfrigérateur"),
    COFFRE_FORT("Coffre-fort");

    private final String libelle;

}
