package com.cancun.structure;

import lombok.AllArgsConstructor;
import lombok.Generated;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TypeLogement {
    CHAMBRE_DOUBLE("CH2X","Chambre Double"),
    CHAMBRE_LITS_JUMEAUX("CHLJ","Chambre Lits Jumeaux"),
    SUITE_JUNIOR("SUJR", "Suite Junior"),
    CHAMBRE_TRIPLE("CH3X", "Chambre Triple"),
    CHAMBRE_FAMILIALE("CHFL","Chambre Familiale"),
    CHAMBRE_SIMPLE("CHSP","Chambre Simple");

    private final String code;
    private final String libelle;
}
