package com.cancun.structure;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Description {
    CHAMBRE_SIMPLE("1 lit simple Lits confortables, notés 9 (d'après 104 commentaires) Air-conditioned room featuring an LCD TV, a phone and a private bathroom with shower and hairdryer."),
    CHAMBRE_DOUBLE("1 grand lit double Lits confortables, notés 9 (d'après 104 commentaires) Air-conditioned room featuring an LCD TV, a phone and a private bathroom with shower and hairdryer."),

    CHAMBRE_LIT_JUMEAUX("2 lits simples Lits confortables, notés 9 (d'après 104 commentaires) Air-conditioned room featuring an LCD TV, a phone and a private bathroom with shower and hairdryer."),

    CHAMBRE_SUITE("1 grand lit double Lits confortables, notés 9 (d'après 104 commentaires) This suite has air conditioning."),
    CHAMBRE_TRIPLE("3 lits simples Lits confortables, notés 9 (d'après 104 commentaires) Air-conditioned room featuring an LCD TV, a phone and a private bathroom with shower and hairdryer."),

    CHAMBRE_FAMILIALE("4 lits simples Lits confortables, notés 9 (d'après 104 commentaires) This family room features air conditioning.");

    private final String libelle;

}
