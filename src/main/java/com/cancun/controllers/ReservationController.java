package com.cancun.controllers;

import com.cancun.dto.ReservationDto;
import com.cancun.services.ChambreService;
import com.cancun.services.ReservationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;

@RestController
@RequestMapping(path = "api/cancun")
@Validated
@RequiredArgsConstructor
@Tag(name = "Reservation")
public class ReservationController {

    @Autowired
    private ReservationService reservationService;



    @Autowired
    private ChambreService chambreService;


    @Operation(summary = "Récupérer toutes les réservations stockées dans la base de données")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Détails de toutes les réservations",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404",
                    description = "Page non trouvée",
                    content = @Content)
    })
    @GetMapping(path = "/reservations")
    public ResponseEntity<?> recupererReservations() {
        return reservationService.recupererReservations();
    }


    @Operation(summary = "Effectuer une réservation dans la base de données")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Détails de la réservation enregistrés dans la base de données",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404",
                    description = "Page non trouvée",
                    content = @Content)
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/reservation/effectuer")
    public ResponseEntity<?> effectuerUneReservation(@Valid @RequestBody ReservationDto reservationDto) throws ParseException{
            return reservationService.effectuerUneReservation(reservationDto);

    }


    @Operation(summary = "Annuler la réservation dans la base de données")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Annulation d'une réservation dans la base de données",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404",
                    description = "Page non trouvée",
                    content = @Content)
    })
    @PutMapping(value = "/reservation/annuler")
    public ResponseEntity<?>  annulerReservation(@Valid @RequestParam long id) {
        return  reservationService.annulerReservation(id);
    }


    @Operation(summary = "Modifier la réservation dans la base de données")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Mise à jour des détails de la réservation dans la base de données",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404",
                    description = "Page non trouvée",
                    content = @Content)
    })
    @PutMapping(value = "/reservation/modifier")
    public ResponseEntity<?> updateReservation(@Valid @RequestBody ReservationDto reservationDto) {
       return reservationService.modifierReservation(reservationDto);
    }

    @Operation(summary = "Obtenir les détails d'une réservation particulière dans la base de données.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Détails de la réservation récupérés dans la base de données",
                    content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404",
                    description = "Page non trouvée",
                    content = @Content)
    })
    @GetMapping(value = "/reservation")
    public ResponseEntity<?> afficherReservation(@Valid @RequestParam long id) {
        return reservationService.recupererReservation(id);
    }

}
