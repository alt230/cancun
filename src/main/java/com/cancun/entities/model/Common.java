package com.cancun.entities.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.modelmapper.ModelMapper;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@MappedSuperclass
@Setter
@Getter
public abstract class Common implements Serializable {

    @CreationTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    protected LocalDateTime dateCreation;

    @UpdateTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    protected LocalDateTime dateMisAjour;


    public static LocalDateTime dateToLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }


    public static <S, T> List<T> mapList(List<S> source, Class<T> targetClass) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setSkipNullEnabled(true);
        return source.stream().map(element -> modelMapper.map(element, targetClass)).collect(Collectors.toList());
    }

//    public static <T> T mapObject(Class<T> targetClasse){
//        ModelMapper mapper = new ModelMapper();
//        return mapper.map(targetClasse, T);
//    }

    public static <S, T> T mapObject(S source, Class<T> targetClass) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(source, targetClass);
    }


    /**
     * Transforme la date chaine de caractere en date Date
     *
     * @param DateInString
     * @return Date
     * @throws ParseException
     */
    public static Date formatStringToDate(String DateInString) throws ParseException {
        SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
        return sdformat.parse(DateInString);
    }
}
