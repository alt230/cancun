package com.cancun.entities.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Person extends Common implements Serializable {
    @Column(name = "nom", nullable = false)
    protected String nom;
    @Column(name = "prenom")
    protected String prenom;

    @Column(name = "email")
    protected String email;
    @Column(name = "telephone")
    protected String telephone;
    @Column(name = "civilite")
    protected String civilite;

    @Column(name = "date_arrivee")
    private Date dateArrivee;
}
