package com.cancun.entities;

import com.cancun.entities.model.Common;
import com.fasterxml.jackson.annotation.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ToString
@Builder
public class Reservation extends Common implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "date_reservation", updatable = true, nullable = false)
    @Schema(description = "Date reservation", example = "2023-01-06")
    private LocalDateTime dateReservation;

    @Column(name = "date_debut", updatable = true, nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Schema(description = "Date debut", example = "2023-01-06")
    private LocalDateTime dateDebut;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "date_fin", updatable = true, nullable = true)
    @Schema(description = "Date fin", example = "2023-01-06")
    private LocalDateTime dateFin;

    @Column(name = "annule")
    private Boolean annule=false;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "date_annulation", updatable = true, nullable = true)
    @Schema(description = "Date annulation", example = "2023-01-06")
    private LocalDateTime dateAnnulation;

    private int dureeSejour;

    @ManyToOne(targetEntity = Chambre.class, fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "chambre_id")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = false)
    private Chambre chambre;
}
