package com.cancun.entities;


import com.cancun.entities.model.Common;
import com.cancun.structure.Equipement;
import com.cancun.structure.OptionSalleDeBain;
import com.cancun.structure.Vue;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Chambre extends Common implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String typeLogement;
    private String description;
    private  double superficie;
    private boolean nonFumeur;
    private boolean disponible;
    private int nombreDeJourReservation;
    private float montant;

    @ElementCollection(targetClass = Equipement.class)
    @CollectionTable(name="chambre_equipements")
    @Column(name = "equipement", nullable = true)
    @Enumerated(EnumType.STRING)
    Collection<Equipement> equipements;

    @ElementCollection(targetClass = OptionSalleDeBain.class)
    @CollectionTable(name="chambre_options")
    @Column(name = "option_salle_de_bain", nullable = true)
    @Enumerated(EnumType.STRING)
    Collection<OptionSalleDeBain> optionSalleDeBains;


    @ElementCollection(targetClass = Vue.class)
    @CollectionTable(name="chambre_vues")
    @Column(name = "vue", nullable = true)
    @Enumerated(EnumType.STRING)
    Collection<Vue> vues;
}
