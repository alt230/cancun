package com.cancun.utils;

public class CustumStatus {
    public static final Integer OK = 200;
    public static final Integer INFO = 300;
    public static final Integer ERROR = 900;
    public static final Integer WARNING = 800;
    public static final Integer EXISTE = 700;
    public static final Integer NO_CONTENT = 204;
}

