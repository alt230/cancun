package com.cancun.utils;

public class CustumMessage {
    public static final String OPERATION_SUCCESS = "Opération terminée avec succès";
    public static final String CANCEL_OPERATION_SUCCESS = "Annulation effectuée avec succès";
    public static final String CHAMBRE_INDISPONIBLE ="Chambre indisponible!";
    public static final String LISTE_VIDE = "Aucun résultat trouvé";
    public static final String SELECTIONNER_CHAMBRE = "Veuillez séléctionner une chambre";
    public static final String SELECTIONNER_RESERVATION= "Veuillez séléctionner une reservation";
    public static final String CHAMPS_REQUIS = "Champs requis";
    public static final String DONNEE_INTROUVABLE = "Cette information ne peut pas être trouvée";
    public static final String CHAMPS_OBLIGATOIRE_VIDE = "Veuillez remplir les champs obligatoires";
    public static final String TYPE_INCORRECT = "Veuillez respecter le type de données";
    public static final String SELECTIONNER_AU_MOINS_UN_CLIENT = "Veuillez selectionner au moins un client";

    public static final String CHAMBRE_INTROUVABLE = "Cet identifiant de chambre n'existe pas";
    public static final String SEJOUR_SUPERIEUR_A_03_JOURS = "Désolé le séjour ne peut pas être supérieur à 3 jours.";
    public static final String DATE_RESERVATION_SUPERIEUR_A_30_JOURS = "Désolé impossible de réserver plus de 30 jours à l'avance.";
    public static final String RESERVATION_INTROUVABLE = "Cet identifiant de reservation n'existe pas";
    public static final String RESERVATION_DEJA_ANNULER = "Désolé, cette reservation a déjà été annulée";

}
